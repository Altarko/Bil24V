(function() {
  'use strict';

  angular.module('app.eventpanel', [
    'app.core',
    'app.widgets'
  ]);
})();
