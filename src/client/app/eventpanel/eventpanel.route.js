(function() {
  'use strict';

  angular
    .module('app.eventpanel')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'eventpanel',
        config: {
          url: '/',
          templateUrl: 'app/eventpanel/eventpanel.html',
          controller: 'EventPanelController',
          controllerAs: 'vm',
          title: 'eventpanel'
        }
      }
    ];
  }
})();
