(function() {
  'use strict';

  angular
    .module('app.eventpanel')
    .controller('EventPanelController', EventPanelController);

  EventPanelController.$inject = ['$q', 'dataservice', 'logger'];
  /* @ngInject */
  function EventPanelController($q, dataservice, logger) {
    var vm = this;

    vm.actionList = [
      {
        "actionId": 29,
        "actionName": "Billings Place",
        "duration": 83,
        "fullActionName": "Autumn Rivers",
        "description": "Exercitation Exercitation Exercitation Exercitation Exercitation Exercitation Exercitation Exercitation Exercitation Exercitation Exercitation Exercitation Exercitation laboris veniam do labore exercitation Lorem dolore ex nostrud dolore consectetur do. Qui adipisicing sint ad ullamco nisi dolor sint dolore ea irure reprehenderit. Quis ea cupidatat nulla pariatur ad nulla ea elit laboris officia veniam. In tempor elit incididunt dolore dolor culpa dolor eu laborum sint eiusmod. Adipisicing esse ea dolore dolore non culpa.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 998,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 4,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 35,
        "actionName": "Reeve Place",
        "duration": 52,
        "fullActionName": "Donna Clarke",
        "description": "Laborum sint velit officia pariatur sint. Nostrud aute nisi ad ipsum reprehenderit quis exercitation eu ex. Labore excepteur consequat excepteur fugiat reprehenderit nulla ipsum. Ut reprehenderit incididunt eu id commodo dolor sit voluptate est labore id. Anim nostrud laborum Lorem non incididunt adipisicing. Sint anim non incididunt sit sit proident et ad dolore esse enim est cillum sit. Adipisicing et dolore cillum exercitation incididunt est exercitation elit amet.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 1625,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 5,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 25,
        "actionName": "Rock Street",
        "duration": 65,
        "fullActionName": "Battle Downs",
        "description": "Fugiat sit exercitation exercitation deserunt in officia sint excepteur consectetur dolor reprehenderit. Duis veniam tempor commodo Lorem adipisicing sint qui adipisicing ullamco nostrud eiusmod voluptate laboris. Adipisicing reprehenderit mollit aliqua laborum amet do voluptate. Eiusmod eu proident ipsum aliquip est pariatur incididunt reprehenderit magna. Esse magna culpa consequat laboris eu ut officia nostrud minim. Consectetur id in dolor excepteur irure cupidatat ut pariatur in duis reprehenderit magna exercitation deserunt.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 3740,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 5,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 38,
        "actionName": "Louisa Street",
        "duration": 119,
        "fullActionName": "Laverne Cherry",
        "description": "Labore minim occaecat duis ea aliqua cupidatat reprehenderit. Nostrud in labore ad minim sunt. Consectetur amet laborum laborum dolore esse exercitation quis irure ad anim. Dolore ipsum nulla pariatur laborum occaecat do sit amet in nulla dolore irure pariatur velit. Aute fugiat reprehenderit nisi culpa.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 2931,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 2,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 31,
        "actionName": "Gain Court",
        "duration": 73,
        "fullActionName": "Noreen Perry",
        "description": "Ea irure dolor culpa proident velit quis id non est mollit exercitation. Cillum quis sunt consectetur cillum et sunt mollit incididunt dolore eu nostrud ex in. Consectetur eu ipsum eiusmod excepteur dolore dolor ipsum magna irure tempor. Minim adipisicing occaecat velit anim labore incididunt qui deserunt sunt tempor veniam amet.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 575,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 2,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 39,
        "actionName": "Stratford Road",
        "duration": 51,
        "fullActionName": "Davidson Lambert",
        "description": "Mollit nisi occaecat sunt ullamco veniam cillum deserunt nulla sint consequat proident. Culpa cupidatat ea sit excepteur ad sint dolore commodo sint ipsum. Magna quis eu dolor sit ullamco. Fugiat eu commodo aliquip deserunt cupidatat. Id deserunt aliquip id magna pariatur ad quis.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 830,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 5,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 28,
        "actionName": "Goodwin Place",
        "duration": 95,
        "fullActionName": "Petersen Yates",
        "description": "Lorem cupidatat et officia officia officia officia officia officia officia officia offi ciaofficiavelit velit. Exercitation non ea exercitation ut dolore do cillum laboris. Laboris mollit pariatur anim anim sint consequat ex reprehenderit cillum proident ullamco. Nulla velit voluptate minim consectetur consectetur.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 2391,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 5,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 33,
        "actionName": "Bank Street",
        "duration": 88,
        "fullActionName": "Cortez Hanson",
        "description": "Proident laborum ipsum aliquip Proident laborum ipsum aliquip Proident laborum ipsum aliquip incididunt. Exercitation consequat tempor culpa pariatur excepteur. Est velit eu consequat aute dolor fugiat culpa quis anim sit laborum veniam occaecat culpa. Ea occaecat excepteur dolore Lorem enim sunt pariatur. Sint irure fugiat ad laboris ex dolore magna et Lorem aute qui. Ex ea pariatur sit magna.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 2014,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 5,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 36,
        "actionName": "Freeman Street",
        "duration": 101,
        "fullActionName": "Sellers Saunders",
        "description": "Cupidatat fugiat dolore anim nisi mollit pariatur nisi velit aliquip. Labore do commodo nostrud aute non magna voluptate velit dolor in. Consequat ipsum pariatur esse cupidatat consequat exercitation proident incididunt do. Excepteur do est commodo qui.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 1735,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 4,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 37,
        "actionName": "Driggs Avenue",
        "duration": 58,
        "fullActionName": "Grimes Santiago",
        "description": "Et eiusmod adipisicing qui id in esse exercitation ex ex do pariatur pariatur. Proident duis voluptate ut consectetur commodo velit aute. Duis nulla ullamco sint est fugiat voluptate magna laboris duis do.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 1826,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 0,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 24,
        "actionName": "Llama Court",
        "duration": 119,
        "fullActionName": "Tamika Holt",
        "description": "Consectetur consequat tempor sunt dolore deserunt amet magna Consectetur consequat tempor sunt dolore deserunt amet magna incididunt eiusmod amet dolore dolore. Fugiat officia ut quis id aliqua aliquip pariatur magna aliquip eu dolor. Non sunt tempor excepteur commodo laboris sit quis veniam ipsum commodo nulla irure veniam aute.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 3053,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 1,
        "actionEventTime": "19:00"
      },
      {
        "actionId": 35,
        "actionName": "Vandervoort Avenue",
        "duration": 108,
        "fullActionName": "Vilma Gilliam",
        "description": "Magna deserunt commodo officia est in cillum consequat sunt quis reprehenderit nulla veniam. Pariatur sint id ad amet exercitation. Do officia voluptate sint ea veniam non elit consequat mollit aute magna culpa ad ullamco. Consequat irure occaecat in sunt. Sint quis consectetur esse ullamco do deserunt culpa aute deserunt voluptate consequat. Commodo commodo magna ullamco non duis consectetur adipisicing quis.\r\n",
        "smallPosterUrl": "http://placehold.it/320x335/fff",
        "minSum": 3124,
        "firstEventDate": "15.05.2016",
        "lastEventDate": "20.05.2016",
        "rating": 1,
        "actionEventTime": "19:00"
      }
    ];

    vm.getNumber = getNumber;

    activate();

    function activate() {

    }

    function getNumber(num) {
      if (num > 5) {
        num = 5;
      }
      return new Array(num);
    }

  }
})();
