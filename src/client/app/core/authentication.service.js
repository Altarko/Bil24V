(function () {
  'use strict';

  angular
    .module('app.core')
    .factory('Authservice', Authservice);

  Authservice.$inject = ['$http', '$q', 'api', 'currentUser'];
  /* @ngInject */

  function Authservice($http, $q, api, currentUser) {

    var apiHost = api.url;

    var service = {
      login: login
    };

    return service;


    function login() {

      var data = {
        command: 'AUTH',
        src: 3,
        versionCode: '1.0'
      };

      if (!currentUser.profile.userId && !currentUser.profile.sessionId) {
        console.log('!');
        return $http({
          method: 'POST',
          url: apiHost,
          data: (data)
        })
          .then(success)
          .catch(fail);
      } else {
        return $q.when(currentUser.profile);
      }


      function success(response) {
        console.log(response);
        currentUser.setProfile(response.data.userId, response.data.sessionId);
        return response.data;
      }

      function fail() {
        return exception.catcher('XHR Failed for getPeople')(e);
      }
    }

  }

})();
