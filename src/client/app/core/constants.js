/* global toastr:false, moment:false */
(function() {
  'use strict';

  angular
    .module('app.core')
    .constant('toastr', toastr)
    .constant('moment', moment)
    .constant('api', {
      url: 'https://api.bil24.ru:1240/json'
      //realUrl: 'https://api.bil24.ru:1259/json'
    });
})();
