/**
 * Сервис для работы с локальным хранилищем
 */

(function () {
  'use strict';

  angular
    .module('app.core')
    .factory('localStore', localStore);

  localStore.$inject = ['$window'];
  /* @ngInject */
  function localStore($window) {
    var store = $window.localStorage;

    var service = {
      add: add,             // добавить ключ
      get: get,             // получить значение по ключу
      remove: remove        // удалить ключ
    };

    return service;

    /**
     * Добавить ключ
     * @param key
     * @param value
     */
    function add(key, value) {
      value = angular.toJson(value);
      store.setItem(key, value);
    }

    /**
     * Получить значение по ключу
     * @param key
     */
    function get(key) {
      var value = store.getItem(key);
      if (value) {
        value = angular.fromJson(value);
      }
      return value;
    }

    /**
     * Удалить ключ
     * @param key
     */
    function remove(key) {
      store.removeItem(key);
    }

  }

})();
